dat <- scan('dubr1820.ion', character()) #cijela datoteka
N <- length(dat)        #velieina datoteke
g <- c(1:1)        #prvi broj satelita
g2 <- c(1:1)       #drugi broj satelita

k <- 0
i <- 1        

tablica <- NULL


while (i <= N){
  if(dat[i]==0){
    i <- i+1
    rbind(tablica, data.frame(time = k, SAT="unknown", TEC="unknown")) -> tablica
  }else{
    
    #gledaj da li druga rijee u liniji odgovara ID-u satelita
    
    idcode <- strsplit(dat[i+1], split = "", fixed = TRUE)  #ako je split prazan onda gleda svaki znak odvojeno
    id <- substring(idcode, 4, 4)  #c(\G)
    
    if (id == "G"){  #ako se radi o satelitu onda eitat slj n rijeei kao TEC
      n <- as.integer(dat[i])   #broj(kolieina) satelita
      p <- n  
      
      a <- c(1:1)
      for (l in 1:n){
        a[l] <- as.character(dat[i+l])
      }
    } else {
      p <- 0
    }
    
    sat <- c(1:1)      #sprema broj satelita
    tec <- c(1:1)      #sprema tec vrijednost
    for (j in 1:n){
      #ueitavanje novih linija TEC
      sat[j] <- a[j]
      tec[j] <- as.double(dat[i+j+p])
    }
    
    i <- i + n + p + 1
    
    rbind(tablica, data.frame(time = k, SAT=sat, TEC=tec)) -> tablica
    
  }
  
  k <- k + 1  #new id to be mapped to time
  
}

write.table(tablica, file="tablica.txt", quote = FALSE, sep = "\t", eol = "\r\n", row.names=FALSE, qmethod = "double")
